package homework6;

public class NarcissisticThreads {

    public static void main(String[] args) {

        ExtendedThread thread = new ExtendedThread("just thread");
        thread.start();

        Thread thread1 = new Thread(new ImplementedThread("simply thread"));
        thread1.start();

    }

}


class ExtendedThread extends Thread{

    ExtendedThread(String name){
        super(name);
    }

    public void run(){

        final int LIFE_TIME = 5000; 
        final int SLEEP_TIME = 500;

        long currentTime = System.currentTimeMillis();
        while (  System.currentTimeMillis() < currentTime + LIFE_TIME ) {

            System.out.println( this.getName() );

            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                System.out.println(this.getName() + " has been interrupted");
            }
        }

    }

}

class ImplementedThread implements Runnable{

    private String name ;

    ImplementedThread(String name){
        this.name = name;
    }

    @Override
    public void run() {

        final int LIFE_TIME = 5000;
        final int SLEEP_TIME = 500;

        long currentTime = System.currentTimeMillis();

        while (  System.currentTimeMillis() < currentTime + LIFE_TIME ) {

            System.out.println( name );

            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                System.out.println(name + " has been interrupted");
            }
        }

    }

}
