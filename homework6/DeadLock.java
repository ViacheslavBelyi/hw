package homework6;

public class DeadLock {
    public static void main(String args[]) {

        Person John = new Person("John");
        Person Jim = new Person("Jim");

        John.setInterlocutor(Jim);
        Jim.setInterlocutor(John);

        John.start();
        Jim.start();

    }

}

class Person extends Thread{

    private Person interlocutor ;

    Person( String name){
        this.setName(name);
    }

    public void setInterlocutor(Person interlocutor) {
        this.interlocutor = interlocutor;
    }

    @Override
    public  void run() {
        sayHello();
    }

    public synchronized void sayHello(){
        System.out.println("Hello, " + interlocutor.getName());

        interlocutor.sayBye();

        System.out.println("Good talk!");
    }

    public synchronized void sayBye(){
        System.out.println("Goodbye, " + interlocutor.getName());
    }

}