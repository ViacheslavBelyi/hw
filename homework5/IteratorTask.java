package homework5;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class IteratorTask {

    public static void main(String[] args) {

        Object[] mas = {"hello", 4, new RuntimeException("never used"), "world", '\n'};
        Iterator iterator = new SimpleIterator(mas);

        while ( iterator.hasNext() ) {
            System.out.println(iterator.next());
        }


        LinkedList<Object> list = new LinkedList<>();
        list.add("2");
        list.add('5');
        list.add(6);

        Iterator listIterator = new SimpleIterator(list);

        while ( listIterator .hasNext() ) {
            System.out.println(listIterator.next());
        }

        // listIterator.next();  // test exception
    }

}

interface Iterator{
    public boolean hasNext();
    public Object next();
}

interface Collection{
    public Iterator createIterator();
}

class SimpleCollection implements Collection{

    private Object[] array;

    SimpleCollection(Object[] objects){
        array = objects;
    }

    SimpleCollection(LinkedList<Object> list){
        array = new Object[list.size()];

        java.util.Iterator it = list.iterator();

        for (int i = 0; it.hasNext(); i++) {
            array[i] = it.next();
        }

    }

    @Override
    public Iterator createIterator(){
        return new SimpleIterator(array);
    }

    Object[] getArray(){
        return array;
    }

}


class SimpleIterator implements Iterator{

    private SimpleCollection collection;
    private int index = 0;
    private int collectionLength;

    SimpleIterator(Object[] array){
        collection = new SimpleCollection(array);
        collectionLength = array.length;
    }

    SimpleIterator(LinkedList<Object> list){
        collection = new SimpleCollection(list);
        collectionLength = list.size();
    }

    @Override
    public boolean hasNext() throws IllegalArgumentException {
        if (index < 0 || collectionLength < 0){
            throw new IllegalArgumentException("indexes can`t be negative");
        }

        return index < collectionLength;
    }

    @Override
    public Object next() throws NoSuchElementException {
        Object object;

        if (index < collectionLength) {
            object = collection.getArray()[index];
        } else {
            throw new NoSuchElementException("no such item");
        }

        index+=1;

        return object;
    }

}