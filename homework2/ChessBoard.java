package homework2;

public class ChessBoard {

    static void print(Object object){
        System.out.print(object);
    }

    public static void main(String[] args){
        char[][] board=createBoard(12);

        printBoard(board);
    }

    private static void printBoard(char[][] board){
        for (int i=0; i<board.length; i++){
            for(int j=0; j<board[i].length; j++){
                print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static char[][] createBoard(int size){
        char[][] board= new char[size][size];

        for(int i=0; i<board.length; i++){
            for(int j=0; j<board[i].length; j++){
                board[i][j]=getChar(i,j);
            }
        }

        return board;
    }

    private static char getChar(int i, int j){
        final char SELL_1 = 'W';
        final char SELL_2 = 'B';

        char outChar=SELL_1;

        if( (i+j)%2 == 1)
            outChar = SELL_2;

        return outChar;

    }





}
