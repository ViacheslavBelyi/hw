package homework2;

import java.util.Random;

public class WithoutIfs2 {

    public static void main(String[] args) {

        final int NUMBER_OF_OPTION=5;

        Random random = new Random();
        int option = random.nextInt(NUMBER_OF_OPTION);
        Performer[] performers=new Performer[NUMBER_OF_OPTION];

        performers[0]=new Performer(){
            @Override
            public void doSome() {
                do1();
            }
        };
        performers[1]=new Performer(){
            @Override
            public void doSome() {
                do2();
            }
        };
        performers[2]=new Performer(){
            @Override
            public void doSome() {
                do3();
            }
        };
        performers[3]=new Performer(){
            @Override
            public void doSome() {
                do4();
            }
        };
        performers[4]=new Performer(){
            @Override
            public void doSome() {
                do5();
            }
        };

        performers[option].doSome();

    }
}

