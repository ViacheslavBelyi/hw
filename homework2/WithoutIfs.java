package homework2;

import java.util.Random;


public class WithoutIfs {

    public static void main(String[] args) {

        Random random = new Random();
        Dice.value = random.nextInt(10);

        System.out.println(Dice.value);

        try {
            assert !(Dice.value == 0);
        } catch (AssertionError ae){
            System.out.println("Не повезло");
        }

        try {
            assert !(Dice.value == 1);
        } catch (AssertionError ae){
            System.out.println("Не повезло 1");
        }

        try {
            assert !(Dice.value == 2);
        } catch (AssertionError ae){
            System.out.println("Не повезло 2");
        }

        try {
            assert !(Dice.value == 3);
        } catch (AssertionError ae){
            System.out.println("Не повезло 3");
        }

        try {
            assert !(Dice.value == 4);
        } catch (AssertionError ae){
            System.out.println("Не повезло 4");
        }

        try {
            assert !(Dice.value == 5);
        } catch (AssertionError ae){
            System.out.println("Не повезло 5");
        }

        try {
            assert !(Dice.value == 6);
        } catch (AssertionError ae){
            System.out.println("Повезло 6");
        }

        try {
            assert !(Dice.value == 7);
        } catch (AssertionError ae){
            System.out.println("Повезло 7");
        }

        try {
            assert !(Dice.value == 8);
        } catch (AssertionError ae){
            System.out.println("Очень повезло");
        }

        try {
            assert !(Dice.value == 9);
        } catch (AssertionError ae){
            System.out.println("Удача на твоей стороне!");
        }


    }


    public static class Dice{

        public static int value;

    }



}
