package homework2;

public abstract class Performer{


    protected void do1(){
        System.out.println("action #1");
    }

    protected void do2(){
        System.out.println("action #2");
    }

    protected void do3(){
        System.out.println("action #3");
    }

    protected void do4(){
        System.out.println("action #4");
    }

    protected void do5(){
        System.out.println("action #5");
    }

    public abstract void doSome(); /*function for heirs */

}

