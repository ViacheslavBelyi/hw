package homework4;


import java.util.HashMap;
import java.util.Scanner;

public class MarioGame {

    static void print(Object object){
        System.out.print(object);
    }

    public static void main(String[] args){

        Mario mario = new Mario();

        Scanner scanner = new Scanner(System.in);

        print("switch action : \n\n");
        print(  "1 - jump    \n" +
                "2 - sit     \n" +
                "3 - go left \n" +
                "4 - go right\n" +
                "5 - shot    \n" +
                "6 - pause   \n" +
                "7 - resume  \n" +
                "8 - salto   \n" +
                "0 - quit    \n");

        int command = scanner.nextInt();

        while (command != 0 ){
            mario.respondToCommand(command);
            command = scanner.nextInt();
        }

        print("game over \n");
    }

}

class Mario{

    interface Action{

        void doAction();
    }

    private HashMap<Integer,Action> actionMap;

    Mario(){

        actionMap = new HashMap<>();

        actionMap.put(1, () -> {
            System.out.println("jump");
        });
        actionMap.put(2, () -> {
            System.out.println("sit");
        });
        actionMap.put(3,() -> {
            System.out.println("goLeft");
        });
        actionMap.put(4,() -> {
            System.out.println("goRight");
        });
        actionMap.put(5,() -> {
            System.out.println("shot");
        });
        actionMap.put(6,() -> {
            System.out.println("pause");
        });
        actionMap.put(7,() -> {
            System.out.println("resume");
        });
        actionMap.put(8,() -> {
            System.out.println("salto");
        });

    }

    public void respondToCommand(int command){

        if(actionMap.containsKey(command)) {
            actionMap.get(command).doAction();
        }else{
            System.out.println("wrong command");
        }
    }

}

