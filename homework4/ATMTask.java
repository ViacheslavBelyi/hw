package homework4;

public class ATMTask{

    public static void main(String[] args){

        ATM atm = new ATM();

        atm.pressButton();
        atm.getMoney();
        atm.getMoney();
        atm.pressButton();
        atm.pressButton();
    }
}

class ATM {

    private State state = new PoorState();

    public void getMoney(){
        state.getMoney(this);
    }

    public void pressButton(){
        state.pressButton(this);
    }

    void setState(State state) {
        this.state = state;
    }

}

interface State{
    void getMoney(ATM atm);
    void pressButton(ATM atm);
}

class PoorState implements State{

    @Override
    public void getMoney(ATM atm) {
        atm.setState(new RichState());

        System.out.println("thanks for money");
    }

    @Override
    public void pressButton(ATM atm) {
        System.out.println("no money no honey");
    }
}

class RichState implements State{


    @Override
    public void getMoney(ATM atm) {
        System.out.println("it's not necessary, but I don't mind");
    }

    @Override
    public void pressButton(ATM atm) {
        atm.setState(new PoorState());

        System.out.println("Get the result");
    }
}


